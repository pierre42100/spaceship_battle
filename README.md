# Initial project

I took the git project : https://github.com/andrew-r-king/sfml-vscode-boilerplate to make SFML work on my PC.

# Building
1. Install VSCode  (https://code.visualstudio.com/)
2. Run `sudo apt install -y libsfml-dev`
3. Open the project in VSCode, and press F5

# Resume

Spaceship-battle is a simple game made with C++ and SFML.

# Credit

Background, assets, sprites images where taken from opengameart.org
Precise credits will be written.