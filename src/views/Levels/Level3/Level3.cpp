#include "views/Levels/Level3/Level3.hpp"
#include "views/Menu/Menu.hpp"

Level3::Level3(){
	sf::Clock clock,clockEnnemies, clockPlayer1, clockRound, clockPowerUp, clockShield, clockFlash;
	//Window
	window.create(sf::VideoMode(900, 600), "SpaceBattleship");
	window.setFramerateLimit(30);
	sf::Vector2u windowSize = window.getSize();
	srand(time(NULL));

	bool shieldOn = false;
	bool flashOn = false;

	std::vector<sf::Vector2f> laserPositionDirection;
	std::vector<sf::CircleShape> bouclier;
	Asteroids asteroids;
	Laser laser;
	Ennemies ennemies;
	Laser laserShoot;
	std::size_t a = 200, b=100,c=5;
	laser.getLaser().reserve(a); asteroids.getAsteroids().reserve(b); ennemies.getEnnemies().reserve(500); laserShoot.getLaser().reserve(a);
	//Variable du jeu
	int score = 0, life = 5;


	//Charge l'ecriture de début
	sf::Font font;
	if (!font.loadFromFile("font/CupidDeer.ttf")){
		std::cout<<"Erreur sur le chargement du font CupidDeer";
	}
	std::vector<float> position;
	position.reserve(c);


	//Création Label du Score
	sf::Text text("Score", font, 50);
	text.setPosition(660,20);
	std::string s = std::to_string(score);
	sf::Text textScore(s, font, 50);
	textScore.setPosition(800,20);

	//Création Label de la Life
	sf::Text textLife("Life : ", font, 50);
	textLife.setPosition(50,10);
	std::string sLife = std::to_string(life);
	sf::Text textNbLife(sLife, font, 50);
	textNbLife.setPosition(150, 10);

	//Charge le héro
	Player player1;
	player1.addPlayer("content/PNJ/player3.png");
	player1.setScale(1,1);

	sf::Texture textureUpLife, textureFlash, textureShield;
	textureUpLife.loadFromFile("content/PNJ/pill_green.png"); textureFlash.loadFromFile("content/PNJ/bolt_bronze.png"); textureShield.loadFromFile("content/PNJ/shield_silver.png");

	std::vector<sf::Sprite> vUpLife, vFlash, vShield;


	for(int i = 0; i <10; i++){
		asteroids.addSprite();
	}

	for(int j = 0; j<2;j++){
		ennemies.addSprite();
		float y = rand() % 150;
		position.push_back(y);
	}

	sf::Music music,musicLose,musicCollision,musicBack;
	if (!music.openFromFile("content/sound/sfx_laser1.ogg") || !musicLose.openFromFile("content/sound/sfx_lose.ogg")|| !musicCollision.openFromFile("content/sound/upshort.wav")|| !musicBack.openFromFile("content/sound/SpaceMusic.ogg"))
		std::cout<<"Erreur sur le chargement de la musique";
	musicBack.setVolume(20);
	musicBack.play();
	musicBack.setLoop(true);
	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/blue.png");
	shape.setTexture(&shapeTexture);

	//Mise en place d'un évenement
	sf::Event event;

	//Boucle principale
	while (window.isOpen()){
		while (window.pollEvent(event)){
			if (event.type == sf::Event::Closed)
				window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				if(player1.canMoveLeft(0)){
					player1.move(0,-20,0);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
				if(player1.canMoveRight(0, windowSize)){
					player1.move(0,20,0);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				if(player1.canMoveUp(0,windowSize)){
					player1.move(0,0,-20);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				if(player1.canMoveDown(0,windowSize)){
					player1.move(0,0,20);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				if(clockPlayer1.getElapsedTime().asSeconds()>0.5f){

					if(flashOn){
						laser.addSprite(player1.getPosition(0).x+15,player1.getPosition(0).y-80);
						laser.setScale(1.01f,1.01);
						laser.addSprite(player1.getPosition(0).x+45 ,player1.getPosition(0).y-80);
						laser.setScale(1.01f,1.01);
					}else{
						laser.addSprite(player1.getPosition(0).x+55 ,player1.getPosition(0).y-80);
						laser.setScale(1.01f,1.01);
					}
					clockPlayer1.restart();
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
				VPause pause1;
				int response = pause1.getResponse();
				switch(response){
					case 2:
					window.close();
					createMenu();
					break;
					case 3:
					window.close();
					break;
					default:
					break;
				}
			}
		}

		//Collision
			for(std::size_t i = 0; i<asteroids.size(); i++){
				for(std::size_t j =0; j<laser.size(); j++){
					if(asteroids.getAt(i).getGlobalBounds().intersects(laser.getAt(j).getGlobalBounds())){
						musicCollision.play();
						laser.erase(j);
						asteroids.erase(i);
						++score;
					}
					if(laser.getAt(j).getPosition().y<-50){
						laser.erase(j);
					}
					if(asteroids.getAt(i).getPosition().y>600){
						asteroids.erase(i);
					}
				}
				if(asteroids.getAt(i).getPosition().y > 550){
					asteroids.erase(i);
					musicLose.play();
					--life;
				}
			}

			sLife = std::to_string(life);
			textNbLife.setString(sLife);
			if(life <1){
				musicBack.stop();
				window.close();
				GameOver gameOver1(2,score);
			}

			s = std::to_string(score);
			textScore.setString(s);
			if(score == 50){
				musicBack.stop();
				window.close();
				Win win1(2, score);
			}
			for(std::size_t j = 0; j<laser.size(); j++){
				if(laser.getAt(j).getPosition().y > -100)
					laser.move(j,0,-6);
			}
			for(std::size_t bb = 0; bb<asteroids.size(); bb++){
				asteroids.move(bb);
			}

			// Mise en place des ennemies
			for(std::size_t i = 0; i<ennemies.size(); i++){
				if(clockRound.getElapsedTime().asSeconds() < 30){
				if(ennemies.getAt(i).getPosition().y < position[i]){
					ennemies.move(i);
				}
				else{
					if(clockEnnemies.getElapsedTime().asSeconds() >3.0f){
						sf::Vector2f positionSprite(player1.getPosition(0).x+40, player1.getPosition(0).y);
						laserShoot.addSprite(ennemies.getAt(i).getPosition().x+20, ennemies.getAt(i).getPosition().y+25);
						laserPositionDirection.push_back(positionSprite);
						laserShoot.setScale(1,1);
						laserShoot.changeTexture("content/PNJ/laserGreen2.png");
					}
					for(std::size_t j =0; j<laser.size(); j++){
						if(ennemies.getAt(i).getGlobalBounds().intersects(laser.getAt(j).getGlobalBounds())){
							musicCollision.play();
							laser.erase(j);
							ennemies.erase(i);
							score = score+2;
						}
					}
				}
				}else{
					if(ennemies.getAt(i).getPosition().y < 700)
						ennemies.move(i);
					else{
						for(std::size_t en = 0; en < ennemies.size(); en++){
							ennemies.erase(en);
						}
					}
				}
			}

			if(ennemies.size()==0){
				if(clockRound.getElapsedTime().asSeconds() > 30){
				for(int j = 0; j<2;j++){
					ennemies.addSprite();
					float y = rand() % 150;
					position.push_back(y);
				}
				clockRound.restart();
				}
			}
			if(clockEnnemies.getElapsedTime().asSeconds() > 3.0f)
				clockEnnemies.restart();

		//One asteroids appears every 2 seconds
			if (clock.getElapsedTime().asSeconds() > 2.0f){
				asteroids.addSprite();
				clock.restart();
			}
			for(std::size_t i=0; i<laserShoot.size(); i++){
				// The ennemy's shoot goes to the player1
				if(laserShoot.getAt(i).getPosition().x > laserPositionDirection.at(i).x){
					laserShoot.move(i,-4,4);
				}else if(laserShoot.getAt(i).getPosition().x < laserPositionDirection.at(i).x){
					laserShoot.move(i,4,4);
				}else if(laserShoot.getAt(i).getPosition().x == laserPositionDirection.at(i).x){
					laserShoot.move(i,0,4);
				}
				// The shoot destroys itself when out of the screen
				if(laserShoot.getAt(i).getPosition().y > 600){
					laserShoot.erase(i);
					laserPositionDirection.erase(laserPositionDirection.begin()+i);
				}

				//Player loses life if touched
				if(laserShoot.getAt(i).getGlobalBounds().intersects(player1.getSprite(0).getGlobalBounds())){
						if(!shieldOn){
						musicLose.play();
						laserShoot.erase(i);
						laserPositionDirection.erase(laserPositionDirection.begin()+i);
						--life;
						}
					}
			}

		//Bonus appear
		float x = rand() % 780 + 10;
		float y = rand() % 100 + 450;

		//Flash appears
		if(round((int)clockPowerUp.getElapsedTime().asSeconds()) == 20){
			if(vShield.size()>0)
				vShield.erase(vShield.begin());
			if(vFlash.size()==0){
			sf::Sprite flash(textureFlash);
			flash.setPosition(x,y);
			vFlash.push_back(flash);
			}

		// Medecine appears
		}else if(round((int)clockPowerUp.getElapsedTime().asSeconds()) == 35){
			if(vFlash.size()>0)
				vFlash.erase(vFlash.begin());

			if(vUpLife.size()==0){
			sf::Sprite upLife(textureUpLife);
			upLife.setPosition(x,y);
			vUpLife.push_back(upLife);
			}

		// Shiel appears
		}else if(round((int)clockPowerUp.getElapsedTime().asSeconds()) == 45){
			if(vUpLife.size()>0)
				vUpLife.erase(vUpLife.begin());
			if(vShield.size()==0){
			sf::Sprite shield(textureShield);
			shield.setPosition(x,y);
			vShield.push_back(shield);
			}
			clockShield.restart();
			clockPowerUp.restart();
		}

		//We create the shield if the player got the bonus
		if(round((int)clockShield.getElapsedTime().asSeconds())>10){
			shieldOn = false;
			if(bouclier.size() >0)
			bouclier.erase(bouclier.begin());
		}
		else{
			if(vShield.size()>0){
				if(player1.getSprite(0).getGlobalBounds().intersects(vShield.at(0).getGlobalBounds())){
					shieldOn = true;
					vShield.erase(vShield.begin());
					sf::CircleShape circle;
					circle.setRadius(75);
					circle.setOutlineColor(sf::Color::Black);
					circle.setFillColor(sf::Color::Green);
					circle.setOutlineThickness(5);
					bouclier.push_back(circle);

				}
			}
		}

		//Shield moves with the player
		if (bouclier.size() >0)
				bouclier.at(0).setPosition(player1.getPosition(0).x-20, player1.getPosition(0).y-20);

		//We increment life if the player got the powerup
		if(vUpLife.size()>0){
			if(player1.getSprite(0).getGlobalBounds().intersects(vUpLife.at(0).getGlobalBounds())){
				vUpLife.erase(vUpLife.begin());
				life++;
			}
		}

		//Same for flash, player shoot 2 lasers
		if(vFlash.size()>0){
			if(player1.getSprite(0).getGlobalBounds().intersects(vFlash.at(0).getGlobalBounds())){
				vFlash.erase(vFlash.begin());
				flashOn = true;
				clockFlash.restart();
			}
		}

		if(round((int)clockFlash.getElapsedTime().asSeconds())>20){
			flashOn = false;
		}

		// Draw on window
			window.clear();
		//
			window.draw(shape);
			window.draw(text);
			window.draw(textScore);
			window.draw(textNbLife);
			window.draw(textLife);
			for(std::size_t i=0; i<asteroids.size(); i++)
				window.draw(asteroids.getAt(i));
			for(std::size_t i=0; i<laser.size(); i++)
				window.draw(laser.getAt(i));
			for(std::size_t i=0; i<ennemies.size(); i++){
				window.draw(ennemies.getAt(i));
			}

			for(std::size_t i=0; i<vShield.size(); i++)
				window.draw(vShield.at(i));
			for(std::size_t i=0; i<vUpLife.size(); i++)
				window.draw(vUpLife.at(i));
			for(std::size_t i=0; i<vFlash.size(); i++){
				window.draw(vFlash.at(i));
			}
			for(std::size_t i=0; i<bouclier.size(); i++){
				window.draw(bouclier.at(i));
			}

			for(std::size_t i=0; i<laserShoot.size(); i++){
				window.draw(laserShoot.getAt(i));
			}
			window.draw(player1.getSprite(0));
		//
			window.display();
	}

	}

	int Level3::getResponse(){
		return this->response;
	}

	void Level3::resetScreen(){
			for(std::size_t i=0; i<asteroids.size(); i++)
				if(isOnScreen(asteroids.getAt(i)))
					asteroids.erase(i);
			for(std::size_t i=0; i<ennemies.size(); i++){
				if(isOnScreen(ennemies.getAt(i)))
					ennemies.erase(i);
			}
			for(std::size_t i=0; i<vShield.size(); i++)
				if(isOnScreen(vShield[i]))
					vShield.erase(vShield.begin()+i);
			for(std::size_t i=0; i<vUpLife.size(); i++)
				if(isOnScreen(vUpLife[i]))
					vUpLife.erase(vUpLife.begin()+i);
			for(std::size_t i=0; i<vFlash.size(); i++){
				if(isOnScreen(vFlash[i]))
					vFlash.erase(vFlash.begin()+i);
			}
	}

	bool Level3::isOnScreen(sf::Sprite sprite){
		if(sprite.getPosition().x <800 && sprite.getPosition().x > 0 && sprite.getPosition().y < 600 && sprite.getPosition().y > 0)
			return true;
		return false;
	}