#ifndef LEVEL3_HPP
#define LEVEL3_HPP
#include "views/Levels/Level1/level1.hpp"

class Level3 {
	public:
	sf::RenderWindow window;

	std::vector<sf::Vector2f> laserPositionDirection;
	std::vector<sf::CircleShape> bouclier;
	Asteroids asteroids;
	Laser laser;
	Ennemies ennemies;
	Laser laserShoot;
	std::vector<sf::Sprite> vUpLife, vFlash, vShield;
	Level3();
	int response;
	int getResponse();
	void resetScreen();
	bool isOnScreen(sf::Sprite sprite);

};
// std::vector<sf::Sprite> addEnnemies(std::vector<sf::Sprite> vEnnemies,sf::Texture textureEnnemy);
#endif //LEVEL3_HPP



