#include "VPause.hpp"

VPause::VPause(){
    window.create(sf::VideoMode(900, 600),"Pause");
	sf::Vector2u windowSize = window.getSize();

	int selected = 1;

	sf::Texture textureB1,textureCursor;;
    if(!textureB1.loadFromFile("content/UI/buttonRed.png")||!textureCursor.loadFromFile("content/UI/cursor.png")){
        std::cout<<"Erreur de chargement du bouton";
    }

    sf::Sprite spriteB1(textureB1),spriteB2(textureB1),spriteB3(textureB1), spriteCursor(textureCursor);

	spriteB1.setPosition(sf::Vector2f ((windowSize.x/2 )-100,windowSize.y/2-150));
	spriteB2.setPosition(sf::Vector2f ((windowSize.x/2 )-100,windowSize.y/2));
	spriteB3.setPosition(sf::Vector2f ((windowSize.x/2 )-100,windowSize.y/2+150));
	spriteB1.setScale(0.2,0.2);
	spriteB2.setScale(0.2,0.2);
	spriteB3.setScale(0.2,0.2);


	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/blue.png");
	shape.setTexture(&shapeTexture);

	//Charge l'ecriture de début
	sf::Font font;
    if (!font.loadFromFile("font/kenvector_future_thin.ttf")){
		std::cout<<"Erreur de chargement du bouton";
	}

	//Création Label
    sf::Text text("Pause", font, 40);
	text.setPosition(350,50);
	//Création Label
    sf::Text textB1("Reprendre", font,20);
	textB1.setPosition(spriteB1.getPosition().x+25,spriteB1.getPosition().y+30);
	//Création Label
    sf::Text textB2("Retour Menu", font,20);
	textB2.setPosition(spriteB2.getPosition().x+20,spriteB2.getPosition().y+30);
	//Création Label
    sf::Text textB3("Quitter", font,24);
	textB3.setPosition(spriteB3.getPosition().x+40,spriteB3.getPosition().y+30);

	sf::Music music;
	if (!music.openFromFile("content/sound/click4.ogg"))
    	std::cout<<"Erreur sur le chargement de la musique";
	//Mise en place de la souris
    sf::Vector2f mp;
    mp.x = sf::Mouse::getPosition(window).x;
    mp.y = sf::Mouse::getPosition(window).y;

    while(window.isOpen()){
    	sf::Event event;

    	while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				music.play();
				if (selected >1)
					--selected;
				else selected=3;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				music.play();
				if (selected <3)
					++selected;
				else selected =1;
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				music.play();
				window.close();
				switch(selected){
					case 1:{
						response=1;
						break;
					}
					case 2:
					response = 2;
					break;
					case 3:
					response = 3;
					window.close();
					break;
					default:
					break;
				}
			}

        }


		switch(selected){
			case 1:
			spriteCursor.setPosition(spriteB1.getPosition().x-50, spriteB1.getPosition().y+20);
			break;
			case 2:
			spriteCursor.setPosition(spriteB2.getPosition().x-50, spriteB2.getPosition().y+20);
			break;
			case 3:
			spriteCursor.setPosition(spriteB3.getPosition().x-50, spriteB3.getPosition().y+20);
			break;
			default:
			break;
		}
	window.clear(sf::Color::Black);
	//
	window.draw(shape);
	window.draw(spriteB1);
	window.draw(spriteB2);
	window.draw(spriteB3);
	window.draw(text);
	window.draw(textB1);
	window.draw(textB2);
	window.draw(textB3);
	window.draw(spriteCursor);

	//
	window.display();

	}

}

int VPause::getResponse(){
	return this->response;
}