#include "views/Menu/SelectLevel.hpp"
SelectLevel::SelectLevel(){

	sf::RenderWindow window;
    window.create(sf::VideoMode(900, 600),"Select a level");
	sf::Vector2u windowSize = window.getSize();
	int level = 1;
	int selected = 1;
	//Charger un bouton
	sf::Texture textureArrowLeft,textureArrowRight, textureCadre, textureGo,textureReturn, textureCursor;
    if(!textureArrowLeft.loadFromFile("content/UI/arrowLeft.png")||!textureArrowRight.loadFromFile("content/UI/arrowRight.png")|| !textureCadre.loadFromFile("content/UI/redSheet.png")|| !textureGo.loadFromFile("content/UI/buttonGreen.png") ||(!textureReturn.loadFromFile("content/UI/buttonRed.png"))||!textureCursor.loadFromFile("content/UI/cursor.png")){
		cout<<"Erreur de l'ouverture d'images dans SelectLevel";
    }

	sf::Sprite buttonReturn(textureReturn);
	buttonReturn.scale(0.1,0.1);
    sf::Sprite spriteArrowLeft(textureArrowLeft), spriteArrowRight(textureArrowRight), spriteCadre(textureCadre), spriteGo(textureGo), cursor(textureCursor);
	spriteArrowLeft.setPosition(sf::Vector2f (250,windowSize.y/2-100));
	spriteArrowRight.setPosition(sf::Vector2f (650,windowSize.y/2-100));
	spriteCadre.setPosition(sf::Vector2f (320,windowSize.y/2-150));
	spriteCadre.setScale(sf::Vector2f (1.5,1.5));
	spriteGo.scale(0.15,0.15);
	buttonReturn.setPosition(50,500);

	spriteGo.setPosition(windowSize.x/2-50 ,windowSize.y/2);

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/space2.png");
	shape.setTexture(&shapeTexture);

	//Charge l'ecriture de début
	sf::Font font;
    if (!font.loadFromFile("font/kenvector_future_thin.ttf")){
		cout<<"Erreur de l'ecriture de kenvector_future_thin.ttf";
	}

	//Création Label
    sf::Text text("Select a level", font, 50);
	text.setPosition(250,50);
	//Création Label
    sf::Text textGo("GO", font, 40);
	textGo.setFillColor(sf::Color::Black);
	textGo.setPosition(spriteGo.getPosition().x+40,spriteGo.getPosition().y+10);


	sf::Text textReturn("Back", font, 18);
	textReturn.setPosition(70,510);

	sf::Music music;
	if (!music.openFromFile("content/sound/click4.ogg"))
    	std::cout<<"Erreur sur le chargement de la musique";

	//Création Label du choix du level
	std::string sLevel = std::to_string(level);
    sf::Text textLevel(sLevel, font, 50);
	textLevel.setFillColor(sf::Color::Black);
	textLevel.setPosition(sf::Vector2f ((windowSize.x/2 ),windowSize.y/2-150));


	//Mise en place de la souris
    sf::Vector2f mp;
    mp.x = sf::Mouse::getPosition(window).x;
    mp.y = sf::Mouse::getPosition(window).y;

    while(window.isOpen()){
    	sf::Event event;

    	while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				music.play();
						if (level >1)
							--level;
						else level=3;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
				music.play();
						if (level <3)
							++level;
						else level =1;
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				music.play();
				if (selected >1)
					--selected;
				else selected=2;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				music.play();
				if (selected <2)
					++selected;
				else selected =1;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				music.play();
				window.close();
				switch(selected){
					case 1:{
						if(level==1){
							tutorial();
						}else if(level ==2){
							Level2 level2;
						}else if(level ==3){
							Level3 level3;
						}
						break;
					}
					case 2:
						createMenu();
					break;

					default:
					break;
				}
			}


        }

		switch(selected){
			case 1:
			cursor.setPosition(spriteGo.getPosition().x-50, spriteGo.getPosition().y+20);
			break;
			case 2:
			cursor.setPosition(buttonReturn.getPosition().x-50, buttonReturn.getPosition().y+20);
			break;
			default:
			break;
		}
	sLevel = std::to_string(level);
	textLevel.setString(sLevel);

	window.clear(sf::Color::Black);
	//
	window.draw(shape);
	window.draw(spriteArrowLeft);
	window.draw(spriteArrowRight);
	window.draw(spriteCadre);
	window.draw(spriteGo);
	window.draw(textLevel);
	window.draw(text);
	window.draw(textGo);
	window.draw(buttonReturn);
	window.draw(textReturn);
	window.draw(cursor);

	//
	window.display();

	}

}
