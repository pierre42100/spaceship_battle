#include "GameOver.hpp"
GameOver::GameOver(int levelplayed, int score){
	int selected = 1;
	int level = levelplayed;
	sf::RenderWindow window, window2, window3;
    window.create(sf::VideoMode(900, 600),"You won ! ");

	sf::Texture texture,textureButton,textureCursor;
    if(!texture.loadFromFile("content/button1.png")||!textureButton.loadFromFile("content/UI/buttonRed.png")||!textureCursor.loadFromFile("content/UI/cursor.png")){
        std::cout<<"Erreur.";
    }
	sf::Sprite spriteCursor(textureCursor);
	sf::Sprite spriteButtonRestart(textureButton);
	sf::Sprite spriteButtonMenu(textureButton);
	spriteButtonRestart.scale(0.15,0.15);
	spriteButtonMenu.scale(0.15,0.15);
	spriteButtonRestart.setPosition(360,350);
	spriteButtonMenu.setPosition(360,470);


	//Charge l'ecriture de début
	sf::Font font;
    if (!font.loadFromFile("font/CupidDeer.ttf")){
		std::cout<<"Erreur.";
	}

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/space2.png");
	shape.setTexture(&shapeTexture);

	//Création Label
    sf::Text text("Game Over", font, 80);
	text.setPosition(250,30);

	// Label Score
	sf::Text textScore("Your Score : ", font, 50);
	textScore.setPosition(300,150);
	std::string sScore = std::to_string(score);
    sf::Text textScore2(sScore, font, 50);
	textScore2.setPosition(420,210);

	// Label Buttons
	sf::Text textRetry("Retry", font, 24);
	textRetry.setPosition(spriteButtonRestart.getPosition().x+50,spriteButtonRestart.getPosition().y+20);
	sf::Text textMenu("Menu", font, 24);
	textMenu.setPosition(spriteButtonMenu.getPosition().x+50,spriteButtonMenu.getPosition().y+20);


	sf::Music music;
	if (!music.openFromFile("content/sound/click4.ogg"))
		std::cout<<"Erreur sur le chargement de la musique";

    while(window.isOpen()){
    	sf::Event event;

    	while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				music.play();
				window.close();
				switch(selected){
					case 1:{
						if(level== 1){
							Level1 level1;
						}else if(level == 2){
							Level2 level2;
						}
						else if(level == 3){
							Level3 level3;
						}
						break;
					}
					case 2:
					createMenu();
					break;
					default:
					break;
           	 	}
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				music.play();
				if (selected >1)
					--selected;
				else selected=2;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				music.play();
				if (selected <2)
					++selected;
				else selected =1;
			}
        }


		switch(selected){
			case 1:
			spriteCursor.setPosition(spriteButtonRestart.getPosition().x-50, spriteButtonRestart.getPosition().y+20);
			break;
			case 2:
			spriteCursor.setPosition(spriteButtonMenu.getPosition().x-50, spriteButtonMenu.getPosition().y+20);
			break;
			default:
			break;
		}


	window.clear(sf::Color::Black);
	//
	window.draw(shape);
	window.draw(text);
	window.draw(textScore2);
	window.draw(textScore);
	window.draw(spriteButtonMenu);
	window.draw(spriteButtonRestart);
	window.draw(textMenu);
	window.draw(textRetry);
	window.draw(spriteCursor);

	//
	window.display();
	}
}
