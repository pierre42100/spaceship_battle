#ifndef GAMEOVER_HPP
#define GAMEOVER_HPP
#include "views/Win/Win.hpp"
#include "views/Menu/SelectLevel.hpp"
class GameOver {
	public:
	sf::RenderWindow window;
	GameOver(int levelplayed, int score);
};
#endif //GAMEOVER_HPP