#include "tutorial.hpp"
int tutorial(){

	sf::RenderWindow window;
    window.create(sf::VideoMode(900, 600),"How to play");
	sf::Texture texture,textureKeys;
    if(!texture.loadFromFile("content/UI/buttonGreen.png")|| !textureKeys.loadFromFile("content/keys.png") )
        return 1;
    sf::Sprite sprite(texture);
	sprite.setPosition(sf::Vector2f (350,450));
	sprite.scale(0.15,0.15);

	sf::Sprite spriteKeys(textureKeys);
	spriteKeys.setScale(sf::Vector2f (2,2));
	spriteKeys.setPosition(sf::Vector2f (350,100));

	sf::Music music;
	if (!music.openFromFile("content/sound/click4.ogg"))
    	std::cout<<"Erreur sur le chargement de la musique";

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/darkPurple.png");
	shape.setTexture(&shapeTexture);


	//Charge l'ecriture de début
	sf::Font font;
    if (!font.loadFromFile("font/kenvector_future_thin.ttf")){
		return EXIT_FAILURE;
	}

	//Création Label
    sf::Text textTutorial("How to play ", font, 70);
	textTutorial.setPosition(220,10);

    sf::Text textPressMove("To move ", font, 50);
	textPressMove.setPosition(50,150);

    sf::Text textPressSpace("To shoot ", font, 50);
	textPressSpace.setPosition(50,300);

    sf::Text textGo("Play", font, 18);
	textGo.setPosition(sprite.getPosition().x+20,sprite.getPosition().y+50);


    while(window.isOpen()){
    	sf::Event event;

    	while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}

            if(isSpriteHover(sprite.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true){
                if(event.type == sf::Event::MouseButtonReleased &&  event.mouseButton.button == sf::Mouse::Left){
						music.play();
						window.close();
						Level1 level1;

              		}
            }
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				music.play();
				window.close();
				Level1 level1;
			}

        }
	window.clear(sf::Color::Black);
	//
	window.draw(shape);
	window.draw(textPressMove);
	window.draw(textTutorial);
	window.draw(textPressSpace);
	window.draw(spriteKeys);
	window.draw(sprite);
	window.draw(textGo);

	//
	window.display();

	}
	return 0;

}