#include "Multiplayer.hpp"
#include "../Pause/VPause.hpp"

Multiplayer::Multiplayer(){

	//To do :
	//Set keyboard input for movement of the 2 players
	// z q s d for player1
	// i j k l for player2
	// split in 2 screen


	// Create a window
	sf::RenderWindow window(sf::VideoMode(900, 600), "Jeu de Julia");
	window.setFramerateLimit(30);
	sf::Vector2u windowSize = window.getSize();

	//
	Asteroids asteroids;
	Laser laser;
	//Memory Allocation
	std::size_t a = 200, b=100;
	laser.getLaser().reserve(a); asteroids.getAsteroids().reserve(b);
	sf::Clock clock, clock2, clockPlayer1;
	int coundown = 90;


	int score = 0, life = 3;

	//Charge l'ecriture de début
	sf::Font font;
    if (!font.loadFromFile("font/CupidDeer.ttf")){
		std::cout<<"Erreur";
	}

	//Création Label du Score
    sf::Text text("Score : ", font, 50);
	text.setPosition(660,20);
	std::string s = std::to_string(score);
    sf::Text textScore(s, font, 50);
	textScore.setPosition(820,20);

	// Label du temps écoulé depuis le lancement du level
	std::string stime = std::to_string(int(clock2.getElapsedTime().asSeconds()));
	sf::Text textTime(stime, font, 50);
	textTime.setPosition(420,20);
	sf::Text textTime2("Time : ", font, 50);
	textTime2.setPosition(300,20);

	//Création Label des vies restantes
	sf::Text textLife("Life : ", font, 50);
	textLife.setPosition(50,10);
	std::string sLife = std::to_string(life);
	sf::Text textNbLife(sLife, font, 50);
	textNbLife.setPosition(150, 10);


	sf::Texture texture,texture2;
	for(int i = 0; i <10; i++){
		asteroids.addSprite();
	}

    Player player1;
	player1.addPlayer("content/PNJ/player1.png");
	player1.setPosition(225,490);

	Player player2;
	player2.addPlayer("content/PNJ/player2.png");
	player2.setPosition(675, 490);

	sf::Music music,musicLose,musicBack,musicCollision;
	if (!music.openFromFile("content/sound/sfx_laser1.ogg") || !musicLose.openFromFile("content/sound/sfx_lose.ogg")|| !musicBack.openFromFile("content/sound/SpaceMusic1.ogg")|| !musicCollision.openFromFile("content/sound/upshort.wav"))
    	std::cout<<"Erreur"; // error
	musicBack.setVolume(30);
	musicBack.play();

	srand(time(NULL));
    for(int i=0; i<100; i++){
        float x = rand() % 900;
		float y = rand() % 3000;
        asteroids.getAt(i).setPosition(x, -y);
    }

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/space3.png");
	shape.setTexture(&shapeTexture);

	//Mise en place d'un évenement
	sf::Event event;

	//Boucle principale
	while (window.isOpen()){
		while (window.pollEvent(event)){
			if (event.type == sf::Event::Closed)
				window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				if(player1.canMoveLeft(0)){
					player1.move(0,-20,0);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
				if(player1.canMoveRight(0,windowSize)){
					player1.move(0,20,0);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				if(player1.canMoveUp(0,windowSize)){
					player1.move(0,0,-20);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				if(player1.canMoveDown(0,windowSize)){
					player1.move(0,0,20);
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				if(clockPlayer1.getElapsedTime().asSeconds()>0.5){
					laser.addSprite(player1.getPosition(0).x+45 ,player1.getPosition(0).y-80);
					laser.setScale(1,1);
					clockPlayer1.restart();
				}

			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
				musicBack.setVolume(0);
				VPause pause1;
				int response = pause1.getResponse();
				switch(response){
					case 1:
						musicBack.setVolume(30);
						break;
					case 2:
						window.close();
						createMenu();
						break;
					case 3:
						window.close();
						break;
					default:
						break;
				}
			}
		}


	//One asteroids appears every 2 seconds
	if (clock.getElapsedTime().asSeconds() > 3.0f){
	 	asteroids.addSprite();
		clock.restart();
	}
	//Collision with asteroids
	for(std::size_t i = 0; i<asteroids.size(); i++){
		for(std::size_t j =0; j<laser.getLaser().size(); j++){
			if(asteroids.getAt(i).getGlobalBounds().intersects(laser.getAt(j).getGlobalBounds())){
				musicCollision.play();
				laser.erase(j);
				asteroids.erase(i);
				++score;
			}
			if(laser.getAt(j).getPosition().y<-50){
				laser.erase(j);
			}
			if(asteroids.getAt(i).getPosition().y>600){
				asteroids.erase(i);
			}
		}
		if(asteroids.getAt(i).getPosition().y > 550){
				asteroids.erase(i);
				--life;
				musicLose.play();
			}
	}

	sLife = std::to_string(life);
	textNbLife.setString(sLife);
	if(life <1){
		musicBack.stop();
		window.close();
		GameOver gameOver1(1,score);

	}

	if(clock2.getElapsedTime().asSeconds()<30){
		stime = std::to_string(coundown - (int)(clock2.getElapsedTime().asSeconds()));
		textTime.setString(stime);
	}else{
		stime = std::to_string(coundown - (int)(clock2.getElapsedTime().asSeconds()));
		textTime.setString(stime);
	}
	if(clock2.getElapsedTime().asSeconds()>90){
		musicBack.stop();
		window.close();
		if(score<30)
			GameOver gameOver1(1,score);
		else
			Win win(1,score);


	}



	s = std::to_string(score);
	textScore.setString(s);

		// Les lasers et astéroids bougent à chaque boucle
		for(std::size_t j = 0; j<laser.size(); j++){
			if(laser.getAt(j).getPosition().y > -100)
				laser.move(j,0,-6);
		}

		for(std::size_t bb = 0; bb<asteroids.size(); bb++){
			asteroids.move(bb);
		}
		// Dessine sur la fenêtre
		window.clear();
		//
		window.draw(shape);
		for(std::size_t i=0; i<asteroids.size(); i++)
            window.draw(asteroids.getAt(i));
		for(std::size_t i=0; i<laser.size(); i++)
            window.draw(laser.getAt(i));
		window.draw(player1.getSprite(0));
		window.draw(player2.getSprite(0));

		window.draw(text);
		window.draw(textScore);
		window.draw(textNbLife);
		window.draw(textLife);
		window.draw(textTime);
		window.draw(textTime2);
		//
		window.display();
	}

}
