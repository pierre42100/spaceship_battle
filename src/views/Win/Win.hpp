
#ifndef WIN_HPP
#define WIN_HPP
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "views/Menu/SelectLevel.hpp"

class Win {
	public:
	sf::RenderWindow window;
	Win(int levelplayed, int score);
};
#endif //WIN_HPP

