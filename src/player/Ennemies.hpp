#ifndef ENNEMIES_HPP
#define ENNEMIES_HPP
#include "player/Laser.hpp"
class Ennemies {
  public:
  //Variables
	Laser laserShooted;
	std::vector<bool> shooted;
	bool hasShoot;
	sf::Texture texture;
	std::vector<sf::Sprite> ennemies;
	//Functions
 	void addSprite();
	std::vector<sf::Sprite> getEnnemies();
	sf::Sprite getAt(int i);
	void erase(int i);
	std::size_t size();
	void move(int i);
	Laser getLaser();
	void shootLaser(sf::Sprite player, int i);
	std::vector<sf::Vector2f> laserPositionDirection;
	bool getHasShoot(int i);
	std::vector<sf::Vector2f> getLaserPositionDirection();

};

#endif //ENNEMIES_HPP