#include "Asteroids.hpp"

void Asteroids::addSprite(){
	if (!texture.loadFromFile("content/asteroid.png"))
			std::cout << "Erreur de lecture de l'image d'astéroide.\n";
    	sf::Sprite sprite(texture);
		sprite.setScale(sf::Vector2f (0.4f,0.4));
		float x = rand() % 850;
		float y = rand() % 1000;
// A completer/ A faire marcher
		// while(!comparePosition(x)){
			x = rand() % 850;
		// }
        sprite.setPosition(x, -y);

		asteroids.push_back(sprite);
}

std::vector<sf::Sprite> Asteroids::getAsteroids(){
	return this->asteroids;
}
sf::Sprite Asteroids::getAt(int i){
	return this->asteroids[i];
}

std::size_t Asteroids::size(){
	return this->asteroids.size();
}

void Asteroids::erase(int i){
	asteroids.erase(asteroids.begin()+i);
}

void Asteroids::move(int i){
	asteroids[i].move(0,1.5);
}

bool Asteroids::comparePosition(int x){
	for(std::size_t i=0; i<asteroids.size(); i++){
		if (asteroids[i].getPosition().x >= x){
			if(asteroids[i].getPosition().x - x <=80)
				return false;
		}
		else{
			if( x - asteroids[i].getPosition().x <=80)
				return false;
		}
	}
	return true;
}