#ifndef LASER_HPP
#define LASER_HPP
#include "player/Asteroids.hpp"
class Laser {
  public:
  //Variables
	sf::Texture texture;
	std::vector<sf::Sprite> laser;
	bool touched;
	bool laserEnnemy;
	//Functions
 	void addSprite(int x, int y);
	std::vector<sf::Sprite> getLaser();
	sf::Sprite getAt(int i);
	void erase(int i);
	std::size_t size();
	void move(int i, int x, int y);
	sf::Sprite getLast();
	void setLaserEnnemy(bool condition);
	void setScale(float f1, float f2);
	void changeTexture(std::string path);

};

#endif //LASER_HPP