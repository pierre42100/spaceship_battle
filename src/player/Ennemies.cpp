#include "Ennemies.hpp"

void Ennemies::addSprite(){
	// hasShoot = false;

	if (!texture.loadFromFile("content/PNJ/enemy1.png"))
			std::cout << "Erreur de lecture de l'image d'astéroide.\n";
    	sf::Sprite sprite(texture);
		sprite.setScale(sf::Vector2f (0.7,0.7));
		float x = rand() % 850;
		float y = rand() % 200;
        sprite.setPosition(x, -y);
		ennemies.push_back(sprite);
		shooted.push_back(false);
		// for(int i =0; i < 5; i++)
		// 	laserShooted.addSprite(ennemies[i].getPosition().x+20, ennemies[i].getPosition().y+25););
}

Laser Ennemies::getLaser(){
	return this->laserShooted;
}

std::vector<sf::Sprite> Ennemies::getEnnemies(){
	return this->ennemies;
}
sf::Sprite Ennemies::getAt(int i){
	return this->ennemies[i];
}

std::size_t Ennemies::size(){
	return this->ennemies.size();
}

void Ennemies::erase(int i){
	ennemies.erase(ennemies.begin()+i);
}

void Ennemies::move(int i){
	ennemies[i].move(0,1.5);
}

void Ennemies::shootLaser(sf::Sprite player, int i){
	laserShooted.addSprite(ennemies[i].getPosition().x+20, ennemies[i].getPosition().y+25);
	laserPositionDirection.push_back(player.getPosition());
}

bool Ennemies::getHasShoot(int i){
	return shooted.at(i);
}

std::vector<sf::Vector2f> Ennemies::getLaserPositionDirection(){
	return laserPositionDirection;
}

