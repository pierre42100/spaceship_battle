#ifndef ASTEROIDS_HPP
#define ASTEROIDS_HPP

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>

class Asteroids {
  public:
  //Variables
	sf::Texture texture;
	std::vector<sf::Sprite> asteroids;
	bool touched;
	//Functions
 	void addSprite();
	std::vector<sf::Sprite> getAsteroids();
	sf::Sprite getAt(int i);
	void erase(int i);
	std::size_t size();
	void move(int i);
	bool comparePosition(int x);

};

#endif //ASTEROIDS_HPP