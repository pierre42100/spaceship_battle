#include "Laser.hpp"

void Laser::addSprite(int x, int y){
	sf::Music music;
	music.openFromFile("content/sound/sfx_laser1.ogg");
		if (!texture.loadFromFile("content/PNJ/laser2.png"))
			std::cout << "Erreur de lecture de l'image d'astéroide.\n";
    	sf::Sprite sprite(texture);
		sprite.setScale(sf::Vector2f (0.4f,0.4));
        sprite.setPosition(x, y);

		laser.push_back(sprite);
		music.play();

}

std::vector<sf::Sprite> Laser::getLaser(){
	return this->laser;
}
sf::Sprite Laser::getAt(int i){
	return this->laser[i];
}

sf::Sprite Laser::getLast(){
	return laser[laser.size()-1];
}

std::size_t Laser::size(){
	return this->laser.size();
}

void Laser::erase(int i){
	laser.erase(laser.begin()+i);
}

void Laser::move(int i, int x, int y){
	laser[i].move(x,y);
}

void Laser::setLaserEnnemy(bool condition){
	laserEnnemy = condition;
}

void Laser::setScale(float f1, float f2){
	for(std::size_t i = 0; i < laser.size(); i++ ){
		laser.at(i).setScale(f1,f2);
	}
}

void Laser::changeTexture(std::string path){
	for(std::size_t i = 0; i < laser.size(); i++ ){
		if (!texture.loadFromFile(path))
			std::cout << "Erreur de lecture de l'image d'astéroide.\n";
		laser.at(i).setTexture(texture);
	}
}
