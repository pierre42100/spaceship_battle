#ifndef PLAYER_HPP
#define PLAYER_HPP
#include "player/Laser.hpp"
#include "player/Ennemies.hpp"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>

class Player{
	public:
	sf::Texture texture;
	sf::Sprite sprite;
	std::vector<sf::Sprite> player;
	void addPlayer(std::string pathImage);
	void setPosition(int x,int y);
	bool canMoveLeft(int i);
	bool canMoveRight(int i,sf::Vector2u windowSize);
	bool canMoveUp(int i,sf::Vector2u windowSize);
	bool canMoveDown(int i,sf::Vector2u windowSize);
	sf::Sprite getSprite(int i);
	sf::Vector2f getPosition(int i);
	void setTexture(int i,std::string path);
	void move(int i, int x, int y);
	std::vector<sf::Sprite> getPlayers();
	void setScale(float f1, float f2);
};
#endif //PLAYER_HPP
